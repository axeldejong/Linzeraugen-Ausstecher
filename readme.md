<!--
 Linzeraugen Ausstecher (c) by AJ
 
 Linzeraugen Ausstecher is licensed under a
 Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 
 You should have received a copy of the license along with this
 work. If not, see <http://creativecommons.org/licenses/by-nc-sa/4.0/>.
-->

# Description
A simple coockie cutter for the austrian "Linzer Augen" ("Linzer Eyes"), one of many traditional christmas cookies.

"Linzer Augen" consist of two layers of shortcrust pastry held together by a thin layer of jam (usually ribes). The top has holes cut out, the "Augen".
There are many variations on the holes (stars,faces, ...) but I'll start with the most traditional one: three holes in a regular triangle.