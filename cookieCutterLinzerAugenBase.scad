// Linzeraugen Ausstecher (c) by AJ
// 
// Linzeraugen Ausstecher is licensed under a
// Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
// 
// You should have received a copy of the license along with this
// work. If not, see <http://creativecommons.org/licenses/by-nc-sa/4.0/>.

// cookie cutter parameters
DC=50;
W=15;
DW=10;
HEIGHT=15;
WALL=0.5;

RC=DC/2;
RW=DW/2;
ALPHA=360/W;



// top triangle parameters
TH=1;
TR=5;


// defining modules

// create cylinders for top "triangle"
module cylinders(X, Y, Z, R, H, PC, P) {
    rotate([0, 0, 360/PC*P+6]) {
        translate([X,Y,Z]) {
            cylinder(r=R, h=H, center=true);
        }
    }
}

// hull multiple children with one center
module multiHull(){
    for (i = [1 : $children-1])
        hull(){
            children(0);
            children(i);
        }
}

// build cookie cutter
difference() {
    for (i=[1:1:W]) {
        rotate([0, 0, (ALPHA)*i]) {
            translate([RC-RW, 0, 0]) {
                difference(){
                    cylinder(r=RW, h=HEIGHT, center=true);
                    cylinder(r=RW-WALL, h=HEIGHT, center=true);
                }
            }
        }
    }
    cylinder(r=RC-(RW-1.84), h=HEIGHT, center=true);
}


// create top "triangle"
for (i=[0:1:3]) {
    multiHull(){
        translate([0, 0, HEIGHT/2-TH/2]) {
            cylinder(r=TR, h=TH, center=true);
        }
    cylinders(0, RC-RW, HEIGHT/2-TH/2, TR, TH, 3, i);
    }
}
